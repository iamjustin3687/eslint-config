module.exports = {
	env: {
		node: true,
	},

	extends: [
		"sidvind/es2017",
		"plugin:prettier/recommended",
		"plugin:import/errors",
		"plugin:node/recommended-module",
	],

	plugins: ["prettier", "import", "node"],

	rules: {
		"import/default": "off",
		"import/extensions": "error",
		"import/newline-after-import": "error",
		"import/no-absolute-path": "error",
		"import/no-deprecated": "error",
		"import/no-dynamic-require": "error",
		"import/no-extraneous-dependencies": "error",
		"import/no-mutable-exports": "error",
		"import/no-named-default": "error",
		"import/no-useless-path-segments": "error",
		"import/order": "error",
		"import/no-named-as-default": "error",
		"import/no-named-as-default-member": "error",
		"import/no-duplicates": "error",

		/* this is checked by compiler and without additional configuration does not
		 * work with typescript */
		"node/no-missing-import": "off",

		"consistent-this": "off",
		"no-console": "warn",
		"no-dupe-class-members": "off",
		"no-undef": "off",
		"prettier/prettier": "warn",
		strict: "off",
	},
};
