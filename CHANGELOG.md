# @html-validate/eslint-config changelog

## [1.5.11](https://gitlab.com/html-validate/eslint-config/compare/v1.5.10...v1.5.11) (2020-07-26)

### Bug Fixes

- **deps:** update dependency nunjucks to v3.2.2 ([b0c796d](https://gitlab.com/html-validate/eslint-config/commit/b0c796d9232936bcc4696f8683930baf7baebfb9))

## [1.5.10](https://gitlab.com/html-validate/eslint-config/compare/v1.5.9...v1.5.10) (2020-07-19)

## [1.5.9](https://gitlab.com/html-validate/eslint-config/compare/v1.5.8...v1.5.9) (2020-07-12)

## [1.5.8](https://gitlab.com/html-validate/eslint-config/compare/v1.5.7...v1.5.8) (2020-07-05)

## [1.5.7](https://gitlab.com/html-validate/eslint-config/compare/v1.5.6...v1.5.7) (2020-06-28)

## [1.5.6](https://gitlab.com/html-validate/eslint-config/compare/v1.5.5...v1.5.6) (2020-06-21)

## [1.5.5](https://gitlab.com/html-validate/eslint-config/compare/v1.5.4...v1.5.5) (2020-06-14)

### Bug Fixes

- **deps:** update typescript-eslint monorepo to v3 ([8d4db80](https://gitlab.com/html-validate/eslint-config/commit/8d4db80ab9048bd65078757d24ac396b5856f841))

## [1.5.4](https://gitlab.com/html-validate/eslint-config/compare/v1.5.3...v1.5.4) (2020-06-07)

## [1.5.3](https://gitlab.com/html-validate/eslint-config/compare/v1.5.2...v1.5.3) (2020-05-31)

## [1.5.2](https://gitlab.com/html-validate/eslint-config/compare/v1.5.1...v1.5.2) (2020-05-24)

## [1.5.1](https://gitlab.com/html-validate/eslint-config/compare/v1.5.0...v1.5.1) (2020-05-17)

### Bug Fixes

- add `eslint:fix` script ([0983c8f](https://gitlab.com/html-validate/eslint-config/commit/0983c8f8626e5aa9a9a0febd009ea534bada2e4c))

# [1.5.0](https://gitlab.com/html-validate/eslint-config/compare/v1.4.2...v1.5.0) (2020-05-10)

### Features

- **cli:** autodetect typescript and jest ([3ab7abf](https://gitlab.com/html-validate/eslint-config/commit/3ab7abf202abf08befa2a115dd17d68d7dd628cf))

## [1.4.2](https://gitlab.com/html-validate/eslint-config/compare/v1.4.1...v1.4.2) (2020-05-10)

### Bug Fixes

- **cli:** fix exit code ([71f7627](https://gitlab.com/html-validate/eslint-config/commit/71f7627a541015d59966ea9a82d032bd669ab3fd))

## [1.4.1](https://gitlab.com/html-validate/eslint-config/compare/v1.4.0...v1.4.1) (2020-05-10)

### Bug Fixes

- **cli:** fix shebang ([bda531b](https://gitlab.com/html-validate/eslint-config/commit/bda531bcc12ef72a3580ae632c8c5c7d109126ce))

# [1.4.0](https://gitlab.com/html-validate/eslint-config/compare/v1.3.3...v1.4.0) (2020-05-10)

### Features

- **cli:** cli script will update `package.json` with `eslint` script ([c4f85d8](https://gitlab.com/html-validate/eslint-config/commit/c4f85d8b598382a4ac6fd6eee1b09e44d722111f))

## [1.3.3](https://gitlab.com/html-validate/eslint-config/compare/v1.3.2...v1.3.3) (2020-05-03)

## [1.3.2](https://gitlab.com/html-validate/eslint-config/compare/v1.3.1...v1.3.2) (2020-04-26)

## [1.3.1](https://gitlab.com/html-validate/eslint-config/compare/v1.3.0...v1.3.1) (2020-04-20)

### Bug Fixes

- add missing file ([ab24e31](https://gitlab.com/html-validate/eslint-config/commit/ab24e31956d3f010fb0d5c8eba2004493aeace72))

# [1.3.0](https://gitlab.com/html-validate/eslint-config/compare/v1.2.4...v1.3.0) (2020-04-20)

### Features

- experimental support for `--apply` ([6c40a6f](https://gitlab.com/html-validate/eslint-config/commit/6c40a6f4723dcd07ee33a66ff1008c4be6f778d4))

## [1.2.4](https://gitlab.com/html-validate/eslint-config/compare/v1.2.3...v1.2.4) (2020-04-19)

## [1.2.3](https://gitlab.com/html-validate/eslint-config/compare/v1.2.2...v1.2.3) (2020-04-12)

## [1.2.2](https://gitlab.com/html-validate/eslint-config/compare/v1.2.1...v1.2.2) (2020-04-05)

## [1.2.1](https://gitlab.com/html-validate/eslint-config/compare/v1.2.0...v1.2.1) (2020-03-29)

# [1.2.0](https://gitlab.com/html-validate/eslint-config/compare/v1.1.4...v1.2.0) (2020-03-22)

### Features

- enable eslint-plugin-node ([95d198b](https://gitlab.com/html-validate/eslint-config/commit/95d198bfcf5fe983194a8096dcac9b5f4660e1b6))
- stricter jest rules ([df8fbad](https://gitlab.com/html-validate/eslint-config/commit/df8fbadc79ffb773705691cf23e7bae31cf8b8f6))

## [1.1.4](https://gitlab.com/html-validate/eslint-config/compare/v1.1.3...v1.1.4) (2020-03-01)

## [1.1.3](https://gitlab.com/html-validate/eslint-config/compare/v1.1.2...v1.1.3) (2020-02-23)

## [1.1.2](https://gitlab.com/html-validate/eslint-config/compare/v1.1.1...v1.1.2) (2020-02-16)

## [1.1.1](https://gitlab.com/html-validate/eslint-config/compare/v1.1.0...v1.1.1) (2020-02-13)

### Bug Fixes

- disable `import/default` ([84f6795](https://gitlab.com/html-validate/eslint-config/commit/84f6795fd553bd849f5efb570171fa51333e717f))

# [1.1.0](https://gitlab.com/html-validate/eslint-config/compare/v1.0.10...v1.1.0) (2020-02-11)

### Features

- add eslint-plugin-import ([8233469](https://gitlab.com/html-validate/eslint-config/commit/8233469928c1b12768383fc2c8dd83becebb7def))

## [1.0.10](https://gitlab.com/html-validate/eslint-config/compare/v1.0.9...v1.0.10) (2020-02-09)

## [1.0.9](https://gitlab.com/html-validate/eslint-config/compare/v1.0.8...v1.0.9) (2020-02-02)

## [1.0.8](https://gitlab.com/html-validate/eslint-config/compare/v1.0.7...v1.0.8) (2020-01-26)

## [1.0.7](https://gitlab.com/html-validate/eslint-config/compare/v1.0.6...v1.0.7) (2020-01-12)

## [1.0.6](https://gitlab.com/html-validate/eslint-config/compare/v1.0.5...v1.0.6) (2020-01-05)

## [1.0.5](https://gitlab.com/html-validate/eslint-config/compare/v1.0.4...v1.0.5) (2019-12-29)

## [1.0.4](https://gitlab.com/html-validate/eslint-config/compare/v1.0.3...v1.0.4) (2019-12-25)

### Bug Fixes

- add missing eslint-plugin-jest ([e0459f0](https://gitlab.com/html-validate/eslint-config/commit/e0459f044406530bf97ce7699522b492c9aefdbb))

## [1.0.3](https://gitlab.com/html-validate/eslint-config/compare/v1.0.2...v1.0.3) (2019-12-25)

### Bug Fixes

- back to using regular dependency ([b020a3e](https://gitlab.com/html-validate/eslint-config/commit/b020a3e01e7bff58d69b255c4f65c4af0945b0c1))

## [1.0.2](https://gitlab.com/html-validate/eslint-config/compare/v1.0.1...v1.0.2) (2019-12-25)

### Bug Fixes

- use peerDependency ([41879f2](https://gitlab.com/html-validate/eslint-config/commit/41879f216e2b6a5b9a6f96d7b70daaea2d47ba99))
- **deps:** update typescript-eslint monorepo to v2.13.0 ([de95d83](https://gitlab.com/html-validate/eslint-config/commit/de95d83cacbaa22b80272f427518c094c25e5dc3))

## [1.0.1](https://gitlab.com/html-validate/eslint-config/compare/v1.0.0...v1.0.1) (2019-12-24)

### Bug Fixes

- add missing files ([316fea4](https://gitlab.com/html-validate/eslint-config/commit/316fea448765b5189885f657ee11650db0544fa0))

# 1.0.0 (2019-12-24)

### Features

- initial release refactored from html-validate ([924fbd9](https://gitlab.com/html-validate/eslint-config/commit/924fbd9a5ba0bb460535cb920f970ab38661be14))
