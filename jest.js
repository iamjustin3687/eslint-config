module.exports = {
	env: {
		jest: true,
	},

	plugins: ["jest"],

	extends: ["plugin:jest/recommended", "plugin:jest/style"],

	rules: {
		"jest/consistent-test-it": ["error", { fn: "it" }],
		"jest/no-disabled-tests": "warn",
		"jest/no-duplicate-hooks": "error",
		"jest/no-expect-resolves": "error",
		"jest/no-focused-tests": "warn",
		"jest/no-test-prefixes": "warn",
		"jest/prefer-expect-assertions": "error",
		"jest/prefer-hooks-on-top": "error",
		"jest/prefer-todo": "error",
	},
};
