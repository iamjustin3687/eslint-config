#!/usr/bin/env node

/* eslint-disable no-process-exit -- cli script need to exit with exit code 1 on failure */
import fs from "fs";
import path from "path";
import { ArgumentParser } from "argparse";
import findUp from "find-up";
import { renderNunjucks, Renderer, renderPassthru, Features } from "./render";

interface Options {
	rootDir: string;
	dryRun: boolean;
	changes: number;
}

interface Args {
	mode: "check" | "write";
	jest: null | boolean;
	typescript: null | boolean;
}

/* eslint-disable-next-line @typescript-eslint/no-var-requires -- should require in npm package not bundled */
const pkg = require("../package.json");

const templateDir = path.join(__dirname, "../template");
const FILES = [".eslintrc.json", ".eslintignore"];

async function getRootDir(): Promise<string> {
	const pkgFile = await findUp("package.json");
	if (!pkgFile) {
		console.error("Failed to locate project root");
		process.exit(1);
	}
	return path.dirname(pkgFile);
}

function resolveTemplate(originalFilename: string): [string, Renderer] {
	/* strip leading dot */
	const filename = originalFilename.replace(/^\./, "");

	const literal = path.join(templateDir, filename);
	if (fs.existsSync(literal)) {
		return [literal, renderPassthru];
	}

	const njk = path.join(templateDir, `${filename}.njk`);
	if (fs.existsSync(njk)) {
		return [njk, renderNunjucks];
	}

	throw new Error(
		`Failed to locate template "${originalFilename}" in "${templateDir}"`
	);
}

/**
 * Ensures a file matches template.
 */
async function ensureFile(
	filename: string,
	options: Options,
	features: Features
): Promise<void> {
	const [src, renderer] = resolveTemplate(filename);
	const dst = path.join(options.rootDir, filename);
	const expected = await renderer(src, dst, features);

	/* if the file exists compare the file with the template */
	if (fs.existsSync(dst)) {
		const actual = fs.readFileSync(dst, "utf-8");
		if (actual === expected) {
			return;
		}
	}

	options.changes++;

	if (options.dryRun) {
		console.log(`${filename} - needs update`);
	} else {
		fs.writeFileSync(dst, expected);
		console.log(`${filename} written`);
	}
}

/**
 * Ensures package.json have given script.
 *
 * @param script - Name of script.
 * @param command - Command the script should be set to.
 */
async function ensurePkgScript(
	script: string,
	command: string,
	options: Options
): Promise<void> {
	const dst = path.join(options.rootDir, "package.json");
	const data = fs.readFileSync(dst, "utf-8");
	const pkg = JSON.parse(data);
	if (pkg.scripts && pkg.scripts[script] && pkg.scripts[script] === command) {
		return;
	}

	options.changes++;

	if (options.dryRun) {
		console.log(`package.json - ${script} script needs update`);
	} else {
		if (!pkg.scripts) {
			pkg.scripts = {};
		}
		pkg.scripts[script] = command;
		const output = JSON.stringify(pkg, null, 2);
		fs.writeFileSync(dst, `${output}\n`);
		console.log(`package.json "${script}" script updated`);
	}
	return;
}

async function writeConfig(
	options: Options,
	features: Features
): Promise<boolean> {
	/* ensure all files are up-to-date */
	for (const filename of FILES) {
		await ensureFile(filename, options, features);
	}

	/* ensure package.json is up-to-date */
	await ensurePkgScript("eslint", "eslint .", options);
	await ensurePkgScript("eslint:fix", "eslint --fix .", options);

	/* non-dryrun is always ok */
	if (!options.dryRun) {
		return true;
	}

	if (options.changes > 0) {
		console.error(
			"The files listed needs updating. Use `--write` to write changes automatically."
		);
	}

	return options.changes === 0;
}

async function getFeatures(options: Options, args: Args): Promise<Features> {
	const dst = path.join(options.rootDir, "package.json");
	const data = fs.readFileSync(dst, "utf-8");
	const pkg = JSON.parse(data);
	const deps = { ...pkg.dependencies, ...pkg.devDependencies };
	return {
		jest: args.jest ?? Boolean(deps.jest),
		typescript: args.typescript ?? Boolean(deps.typescript),
	};
}

async function run(args: Args): Promise<never> {
	const rootDir = await getRootDir();
	const options = {
		rootDir,
		dryRun: args.mode === "check",
		changes: 0,
	};
	const features = await getFeatures(options, args);
	const ok = await writeConfig(options, features);
	process.exit(ok ? 0 : 1);
}

const parser = new ArgumentParser({
	version: pkg.version,
	addHelp: true,
	description: pkg.description,
});

parser.addArgument(["-w", "--write"], {
	action: "storeConst",
	constant: "write",
	dest: "mode",
	help: "Write configuration",
});

parser.addArgument(["-c", "--check"], {
	action: "storeConst",
	constant: "check",
	dest: "mode",
	help: "Check if configuration is up-to-date",
});

parser.addArgument(["--disable-jest"], {
	action: "storeConst",
	constant: false,
	dest: "jest",
	help: "Disable Jest support",
});

parser.addArgument(["--disable-typescript"], {
	action: "storeConst",
	constant: false,
	dest: "typescript",
	help: "Disable typescript support",
});

parser.addArgument(["--enable-jest"], {
	action: "storeConst",
	constant: true,
	dest: "jest",
	help: "Enable Jest support",
});

parser.addArgument(["--enable-typescript"], {
	action: "storeConst",
	constant: true,
	dest: "typescript",
	help: "Enable typescript support",
});

const args = parser.parseArgs();

if (!args.mode) {
	parser.printHelp();
	process.exit(1);
}

run(args);
