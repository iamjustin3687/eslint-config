import path from "path";
import { renderNunjucks, Features, renderPassthru } from "./render";

describe("renderPasstru", () => {
	const filename = path.join(__dirname, "../template/eslintignore");

	it("should pass source as-is", async () => {
		expect.assertions(1);
		const result = await renderPassthru(filename);
		expect(result).toMatchSnapshot();
	});
});

describe("renderNunjucks", () => {
	const filename = path.join(__dirname, "../template/eslintrc.json.njk");

	it("should render base template", async () => {
		expect.assertions(2);
		const context: Features = {
			typescript: false,
			jest: false,
		};
		const result = await renderNunjucks(filename, ".eslintrc.json", context);
		expect(result).toMatchSnapshot();
		expect(() => JSON.parse(result)).not.toThrow();
	});

	it("should render template with typescript", async () => {
		expect.assertions(2);
		const context: Features = {
			typescript: true,
			jest: false,
		};
		const result = await renderNunjucks(filename, ".eslintrc.json", context);
		expect(result).toMatchSnapshot();
		expect(() => JSON.parse(result)).not.toThrow();
	});

	it("should render template with jest", async () => {
		expect.assertions(2);
		const context: Features = {
			typescript: false,
			jest: true,
		};
		const result = await renderNunjucks(filename, ".eslintrc.json", context);
		expect(result).toMatchSnapshot();
		expect(() => JSON.parse(result)).not.toThrow();
	});

	it("should render full template", async () => {
		expect.assertions(2);
		const context: Features = {
			typescript: true,
			jest: true,
		};
		const result = await renderNunjucks(filename, ".eslintrc.json", context);
		expect(result).toMatchSnapshot();
		expect(() => JSON.parse(result)).not.toThrow();
	});
});
