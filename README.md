# @html-validate/eslint-config

> HTML-Validate ESLint [shareable](http://eslint.org/docs/developer-guide/shareable-configs.html) config.

## Install

```
npm install --save-dev @html-validate/eslint-config
```

## Usage

In your .eslintrc.json file:

```json
{
  "extends": ["@html-validate"],

  "overrides": [
    {
      "files": "*.ts",
      "extends": ["@html-validate/eslint-config/typescript"]
    },
    {
      "files": "*.spec.[jt]s",
      "extends": ["@html-validate/eslint-config/jest"]
    }
  ]
}
```

## CLI

To automatically write configuration use:

    npx @html-validate/eslint-config --write

To check if configuration is up-to-date use:

    npx @html-validate/eslint-config --check

The tool will autodetect support for:

- Typescript
- Jest

Use `--enable-FEATURE` or `--disable-FEATURE` to manually enable or disable.
