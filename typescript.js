module.exports = {
	parser: "@typescript-eslint/parser",

	plugins: ["@typescript-eslint"],

	extends: [
		"plugin:@typescript-eslint/recommended",
		"plugin:import/typescript",
	],

	rules: {
		"@typescript-eslint/array-type": "off",
		"@typescript-eslint/explicit-function-return-type": [
			"error",
			{
				allowExpressions: true,
			},
		],
		"@typescript-eslint/explicit-member-accessibility": "error",
		"@typescript-eslint/member-delimiter-style": "error",
		"@typescript-eslint/no-inferrable-types": "off",
		"@typescript-eslint/type-annotation-spacing": "error",
	},
};
